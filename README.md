Modified to include python libraries for ARM CPU architecture.

At the moment, python libraries and modules as well as their dependencies are extracted from Debian's APT repo.
It is tailored for ARMv8 and Python 2.7, which is specifically for an inexpensive TV Box based on ARM Amlogic S905x aarch64 and CoreELEC 9.2.1
Because CoreELEC is a closed system that would not allow the use of apt-get and system files are locked as read-only, importing module according to Kodi's documentation here (https://kodi.wiki/view/Python_libraries)

*** This addon will not work with non ARM hardware as the Python libraries are built for ARM only ***

For developers who wish to compile and do the Python C library binding to create libtorrent python libraries for other platforms supported by CoreELEC / Kodi, please refer to:

Libtorrent Python binding
(https://www.libtorrent.org/python_binding.html)
(https://github.com/arvidn/libtorrent)

This Kodi script / module that checks your architecture of Kodi and import the libtorrent binaries accordingly
(https://github.com/DiMartinoXBMC/script.module.libtorrent)

Original source code of this addon (https://framagit.org/StCyr/plugin.video.peertube or https://github.com/StCyr/plugin.video.peertube)

tvhk.master@gmail.com

--------------------------------------------------------------------------------

A kodi addon for watching content hosted on Peertube (http://joinpeertube.org/) 

This code is still proof-of-concept but it works, and you're welcome to improve it.

# Functionalities

* Browse all videos on a PeerTube instance 
* Search for videos on a PeerTube instance (Only supported by 1.0.0-beta10+ instances)
* Select Peertube instance to use (Doesn't work yet)
* Select the preferred video resolution; The plugin will try to play the select video resolution.
If it's not available, it will play the lower resolution that is the closest from your preference.
If not available, it will play the higher resolution that is the closest from your preference.

# User settings

* Preferred PeerTube instance 
* Preferred video resolution
* Number of videos to display per page
* Sort method to be used when listing videos (Currently, only 'views' and 'likes') 

# Limitations

* This addon doesn't support Webtorrent yet. So, it cannot download/share from/to regular PeerTube clients.
The reason is that it uses the libtorrent python libray which doesn't support it yet (see https://github.com/arvidn/libtorrent/issues/223)
* The addon doesn't delete the downloaded files atm. So, it may fills up your disk 

# Requirements

* Kodi 17 or above
* libtorrent python bindings (https://libtorrent.org/). On Debian type `apt install python-libtorrent` as root.
